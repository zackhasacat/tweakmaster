local core = require("openmw.core")
local types = require("openmw.types")
local self = require("openmw.self")
local lastCell = nil
local isWater = false
local stowedLight = nil
local function getEffectApplied(effectId)
    if core.API_REVISION < 30 then
        return false
    end
    local effect = types.Actor.activeEffects(self):getEffect(effectId)
    return effect and effect.magnitude > 0
end
local function getWaterLevel()
    if core.API_REVISION < 30 then
        return 0
    end
    return 10
end
local function getEquipment(actor)
    if core.API_REVISION < 30 then
        return types.Actor.equipment(actor)
    else
        return types.Actor.getEquipment(actor)

    end
end
local function onUpdate(dt)
    if self.cell ~= lastCell then
        isWater = self.cell.hasWater
    end
    local waterWalk = getEffectApplied("waterwalking")
    if (isWater and not waterWalk) then
        local myLight = getEquipment(self)[types.Actor.EQUIPMENT_SLOT.CarriedLeft]
        if (myLight) then
            if self.position.z < getWaterLevel() or types.Actor.isSwimming(self) then
                stowedLight = myLight
                local eq = getEquipment(self)
                eq[types.Actor.EQUIPMENT_SLOT.CarriedLeft] = nil
                types.Actor.setEquipment(self, eq)
            end
        else
            if self.position.z > getWaterLevel() and not types.Actor.isSwimming(self) then
                if (myLight == nil and stowedLight ~= nil) then
                    local eq = getEquipment(self)
                    eq[types.Actor.EQUIPMENT_SLOT.CarriedLeft] = stowedLight
                    types.Actor.setEquipment(self, eq)
                    stowedLight = nil
                end
            end
        end
    elseif  waterWalk and stowedLight then
        local eq = getEquipment(self)
        eq[types.Actor.EQUIPMENT_SLOT.CarriedLeft] = stowedLight
        types.Actor.setEquipment(self, eq)
        stowedLight = nil
    elseif not isWater and stowedLight then
        local eq =getEquipment(self)
        eq[types.Actor.EQUIPMENT_SLOT.CarriedLeft] = stowedLight
        types.Actor.setEquipment(self, eq)
        stowedLight = nil
    end
    lastCell = self.cell
end

return {
    interfaceName = "zhac_TMaster_pext",
    interface = {
        version = 1,
    },
    engineHandlers = {
        onInit = onInit,
        onUpdate = onUpdate,
    },
    eventHandlers = {
    }
}
