local types = require("openmw.types")
local world = require("openmw.world")
local core = require("openmw.core")
local acti = require("openmw.interfaces").Activation
local util = require("openmw.util")
local storage = require("openmw.storage")
local I = require("openmw.interfaces")
local player = nil
local function sendPlayer(plr)
    player = plr
    print(plr.id)
end
local function showMessage(m)
    player:sendEvent("TMShowMessage", m)
end
local espData = require("scripts.tweakmaster.data.output")
local guardActivate = 0
local npcActivate = 0
local skoomaCount = 0
local sugarCount = 0
local customScripts = {
    quickToss = { path = "scripts/tweakmaster/Quicktoss_player.lua", setting = "EnableQuicktoss" },
    preventExt = { path = "scripts/tweakmaster/tmaster_p_preventext.lua", setting = "EnablePreventExtinguish" },
    contextCast = { path = "scripts/tweakmaster/tmaster_p_contextcast.lua", setting = "EnableContextSpell" },
    permaDeath = { path = "scripts/tweakmaster/permadeath_player.lua", setting = "EnablePD" }
}
local ActorCustomScripts = {
    autoLoot = { path = "scripts/tweakmaster/autoloot/autoloot_act.lua", setting = "EnableAutoLoot" }
}
local function removeItem(actor, itemId, count)
    local foundItems = types.Actor.inventory(actor):findAll(itemId)
    for index, item in ipairs(foundItems) do
        if count > item.count then
            item:remove(count)
            return
        else
            count = count - item.count
            item:remove()
        end
        if count == 0 then
            return
        end
    end
end

local function checkActorScripts(actor)
    if actor.type == types.Player then
        for index, value in pairs(ActorCustomScripts) do
            if (actor:hasScript(value.path)  ) then
                actor:removeScript(value.path)
            end
        end
        return
    end
    for index, value in pairs(ActorCustomScripts) do
        if (actor:hasScript(value.path) and not storage.globalSection("SettingsTweakMaster"):get(value.setting)) then
            actor:removeScript(value.path)
        elseif (not actor:hasScript(value.path) and storage.globalSection("SettingsTweakMaster"):get(value.setting)) then
            actor:addScript(value.path)
        end
    end
end
local function compareRandomNumber(inputNumber)
    -- Generate a random number between 1 and 100
    local randomNumber = math.random(1, 100)

    -- Compare the random number with the input number
    return randomNumber > inputNumber
end
local function checkPlayerScripts()
    for index, value in pairs(customScripts) do
        if (player:hasScript(value.path) and not storage.globalSection("SettingsTweakMaster"):get(value.setting)) then
            player:removeScript(value.path)
        elseif (not player:hasScript(value.path) and storage.globalSection("SettingsTweakMaster"):get(value.setting)) then
            player:addScript(value.path)
        end
    end
    for index, act in ipairs(world.activeActors) do
        checkActorScripts(act)
    end
end
local allowListCL = {}
local function onActorActive(actor)
    checkActorScripts(actor)

    if (storage.globalSection("SettingsTweakMaster"):get("DisableCliff")) then
        for index, value in ipairs(allowListCL) do
            if value == actor.id then
                return
            end
        end
        if (actor.type == types.Creature and actor.type.records[actor.recordId].model:lower() == "meshes\\r\\cliffracer.nif") then
            local chance = storage.globalSection("SettingsTweakMasterCliff"):get("CLDespawnChance")
            if chance == nil then
                chance = 100
            end
            if (chance == 100 and actor.enabled == true) then
                actor:remove()
            else
                local genNumber = compareRandomNumber(chance)
                if genNumber then
                    actor:remove()
                else
                    table.insert(allowListCL, actor.id)
                end
            end
        end
    end
end
local function hideSkooma()
    local skoomaOb = types.Actor.inventory(player):find("potion_skooma_01")
    if (skoomaOb) then
        skoomaCount = skoomaOb.count
        skoomaOb:remove()
    end
    local sugarOb = types.Actor.inventory(player):find("ingred_moon_sugar_01")
    if (sugarOb) then
        sugarCount = sugarOb.count
        sugarOb:remove()
    end
end
local function returnSkooma()
    if (skoomaCount > 0) then
        local skoomaOb = world.createObject("potion_skooma_01", skoomaCount)
        skoomaOb:moveInto(types.Actor.inventory(player))
        skoomaCount = 0
    end
    if (sugarCount > 0) then
        local sugarObject = world.createObject("ingred_moon_sugar_01", sugarCount)
        sugarObject:moveInto(types.Actor.inventory(player))
    end
end
local function onUpdate()
    if npcActivate > 0 then
        if core.getGameTime() > npcActivate + 1 then
            npcActivate = 0
            returnSkooma()
        end
    end
end
local function activateNPC(object, actor)
    local actorRecord = object.type.records[object.recordId]
    print("activate")

    if (core.getRealTime() > guardActivate + 10 and types.Player.getCrimeLevel(player) > 0 and actorRecord.class == "guard" and storage.globalSection("TweakMaster"):get("EnableGuardStop")) then
        showMessage("I should maybe mind my own business for now...")
        guardActivate = core.getRealTime()
        return false
    end
    if storage.globalSection("SettingsTweakMaster"):get("EnableAutoSkoomaHide") and actorRecord.race ~= "khajiit" then
        hideSkooma()
        npcActivate = core.getGameTime()
    end
end
acti.addHandlerForType(types.NPC, activateNPC)

local function TweakMasterUpdateSetting(data)
    storage.globalSection("SettingsTweakMaster"):set(data.key, data.value)
    print(data.section)
    checkPlayerScripts()
end
local function onPlayerAdded(player)
    player = player
    checkPlayerScripts()
end
local function onLoad()
    if (storage.globalSection("SettingsTweakMaster"):get("DisableCliff")) then
        for index, value in ipairs(world.activeActors) do
            onActorActive(value)
        end
    end
    if player ~= nil then
        checkPlayerScripts()
    end
end
local placedObjects = {}
local function processCellData(reverse)
    local cellName = "Balmora, Eight Plates"
    if (reverse == nil) then
        reverse = false
    end
    local cellData --= espData["2"].references

    for index, data in pairs(espData) do
        if data.id == cellName then
            cellData = data
            break
        elseif data.id ~= nil then
            print(data.id)
        end
    end
    if (cellData == nil) then
        print("Data for " .. cellName .. "not found")
    end
    local targetCell = world.getCellByName(cellName)
    local cellLoad = world.getCellByName(cellName):getAll()
    for index, value in ipairs(cellLoad) do
        if (value.contentFile == nil) then
            -- value:remove()
        end
    end
    cellLoad = world.getCellByName(cellName):getAll()

    if (reverse == true) then
        for index, id in ipairs(placedObjects) do
            for index, object in ipairs(cellLoad) do
                if object.id == id then
                    print(object.id)
                    object.enabled = false
                    break
                end
            end
        end
    end
    for index, obj in pairs(cellData.references) do
        if (obj.deleted ~= nil) then
            local contentFile = "morrowind.esm"

            if (obj.mast_index == 7) then
                contentFile = "beautiful cities of morrowind.esp"
            end

            local formId = core.getFormId(contentFile, obj.refr_index)

            print(formId)
            local ref = world.getObjectByFormId(formId)
            if (ref:isValid()) then
                ref.enabled = reverse
            else
                print("Not valid:" .. formId)
                print(contentFile)
                print(obj.mast_index)
            end
        elseif not reverse and obj.id ~= nil then
            local id = obj.id
            local pos = util.vector3(obj.translation["1"], obj.translation["2"], obj.translation["3"])
            local rot = util.transform.rotateZ((obj.rotation["3"]))
            local newOb = world.createObject(id)
            table.insert(placedObjects, newOb.id)
            print(newOb.id)
            newOb:teleport(targetCell, pos, rot)
        end
    end
end

return {
    interfaceName = "zhac_TMaster",
    interface = {
        version = 1,
        onInit = onInit,
        data = espData,
        processCellData = processCellData,
    },
    engineHandlers = {
        onInit = onInit,
        onUpdate = onUpdate,
        onLoad = onLoad,
        onPlayerAdded = onPlayerAdded,
        onActorActive = onActorActive,
    },
    eventHandlers = {
        TweakMasterUpdateSetting = TweakMasterUpdateSetting,
        sendPlayer = sendPlayer,
    }
}
