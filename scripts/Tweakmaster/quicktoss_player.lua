local types = require('openmw.types')
local input = require("openmw.input")
local self = require("openmw.self")
local core = require("openmw.core")


local equipdelay = -1
local prevStance = nil
local startTime = 0

local wasOverRiding = false
local prevWeapon = nil

local function findWeapon()

    --find a thrown weapon. In the future, this will pick the object with the highest damage, or the last equipped thrown weapon.
    local inv = types.Actor.inventory(self):getAll(types.Weapon)
    local ret = nil
    for index, weapon in ipairs(inv) do
        if (types.Weapon.record(weapon).type == types.Weapon.TYPE.MarksmanThrown) then
            ret = weapon
        end
    end
    return ret
end

local function findSlot(item)--
    if item.type == types.Armor then
        return types.Armor.records[item.recordId].enchant
    elseif item.type == types.Book then
        return types.Book.records[item.recordId].enchant
    elseif item.type == types.Clothing then
        if (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Amulet) then
            return types.Actor.EQUIPMENT_SLOT.Amulet
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Skirt) then
            return types.Actor.EQUIPMENT_SLOT.Skirt
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Shirt) then
            return types.Actor.EQUIPMENT_SLOT.Shirt
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Shoes) then
            return types.Actor.EQUIPMENT_SLOT.Boots
        elseif (types.Clothing.records[item.recordId].type == types.Clothing.TYPE.Robe) then
            return types.Actor.EQUIPMENT_SLOT.Robe
        end
    elseif item.type == types.Weapon then
        return types.Actor.EQUIPMENT_SLOT.CarriedRight
    end
    print("Couldn't find slot for " .. item.recordId)
    return false
end
local function getEquippedInSlot(slot, actor)
    local equip = types.Actor.getEquipment(actor)
    for index, itemId in pairs(equip) do
        if (index == slot) then
            return itemId
        end
    end
    return nil
end
local function equipItem(itemId, unEquip)
    if (unEquip == nil) then
        unEquip = false
    end
    if (unEquip == true) then
        local equip = types.Actor.getEquipment(self)
        equip[itemId] = nil
        types.Actor.setEquipment(self, equip)
        return
    end
    if (itemId == nil) then
        return nil
    end
    if (itemId.recordId ~= nil) then
        itemId = itemId.recordId
    end
    local inv = types.Actor.inventory(self)
    local item = inv:find(itemId)
    local slot = findSlot(item)
    if (slot) then
        local equip = types.Actor.getEquipment(self)
        if (unEquip) then
            equip[slot] = nil
        else
            equip[slot] = item
        end

        types.Actor.setEquipment(self, equip)
    end
end

local function onUpdate(dt)
    if (equipdelay > 0) then
        equipdelay = equipdelay - dt
    elseif (equipdelay < 0 and equipdelay ~= -1)  then
        if (prevWeapon == "nil") then
            equipItem(types.Actor.EQUIPMENT_SLOT.CarriedRight, true)
        else
            equipItem(prevWeapon)
        end
        prevWeapon = nil
        equipdelay = -1
        if (prevStance ~= nil) then
            types.Actor.setStance(self, prevStance)
            prevStance = nil
        end
    end
    if (input.isKeyPressed(input.KEY.M)) then
        local tweapon = findWeapon()
        if (tweapon ~= nil) then
            if (wasOverRiding == false) then
                startTime = core.getRealTime()
                if (types.Actor.getStance(self) ~= types.Actor.STANCE.Weapon) then
                    prevStance = types.Actor.getStance(self)
                    types.Actor.setStance(self, types.Actor.STANCE.Weapon)
                end
                prevWeapon = getEquippedInSlot(types.Actor.EQUIPMENT_SLOT.CarriedRight, self)
                if (prevWeapon == nil) then
                    prevWeapon = "nil"
                end
                equipItem(tweapon.recordId)
                local controls = require('openmw.interfaces').Controls
                controls.overrideCombatControls(true)--disable the built in combat controls , otherwise self.controls.use is not easy to manipulate.
                print("use")
            elseif (wasOverRiding == true and startTime > 0) then

            end

            wasOverRiding = true
            self.controls.use = 1
        else
            print("No Thrown Weapon")
        end
    end
    if (wasOverRiding == true and startTime > 0 and input.isKeyPressed(input.KEY.M) == false) then
        local waitTime = core.getRealTime() - startTime
        if (waitTime > 0.8) then--Want to make sure to let go only after fully charged
            wasOverRiding = false
            if (prevWeapon ~= nil) then
                print(prevWeapon)
            end
            equipdelay = 1
            self.controls.use = 0
            local controls = require('openmw.interfaces').Controls
            controls.overrideCombatControls(false)--turn back on the normal combat controls
            startTime = 0
        else
            print(waitTime)
            self.controls.use = 1
        end
    end
end

return {
    engineHandlers = {
        onInit = onInit,
        onUpdate = onUpdate,
    },
    eventHandlers  = {

    }
}
