local json = require("dkjson")

local input_file = "InnRooms.json"
local output_file = "output.lua"

local file = io.open(input_file, "r")
local content = file:read("*a")
file:close()

local dataTable, pos, err = json.decode(content)
if not dataTable then
    print("Failed to decode JSON: " .. err)
    return
end

local function writeTableToFile(file, table, level)
    for key, value in pairs(table) do
        file:write(string.rep("  ", level))
        file:write('["' .. tostring(key) .. '"] = ')
        if type(value) == "table" then
            file:write("{\n")
            writeTableToFile(file, value, level + 1)
            file:write(string.rep("  ", level))
            file:write("},\n")
        else
            file:write(json.encode(value) .. ",\n")
        end
    end
end

file = io.open(output_file, "w")
file:write("return {\n")
writeTableToFile(file, dataTable, 1)
file:write("}\n")
file:close()
