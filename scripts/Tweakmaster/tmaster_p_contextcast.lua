local camera = require('openmw.camera')
local core = require('openmw.core')
local input = require('openmw.input')
local util = require('openmw.util')
local self = require('openmw.self')
local types = require('openmw.types')
local ui = require('openmw.ui')
local async = require('openmw.async')
local storage = require('openmw.storage')
local I = require('openmw.interfaces')
local self = require('openmw.self')
local interfaces = require('openmw.interfaces')
local core = require('openmw.core')
local ui = require('openmw.ui')
local camera = require('openmw.camera')
local nearby = require('openmw.nearby')
local self = require('openmw.self')
local types = require('openmw.types')
local Actor = types.Actor
local Player = types.Player
local util = require('openmw.util')
local input = require('openmw.input')
local debug = require('openmw.debug')
local lastStance = 0


local prevSpell = nil
local prevEnch = nil
local tempEnch = nil
local tempSpell = nil

local facedObject = nil
local facedObjectUpdateTime = -1
local distanceToFacedObject = -1
--cure poison when poisoned
--levitate when looking up
local function getObjectCastCost(item)
    local ench   = core.magic.enchantments[item.type.records[item.recordId].enchant].cost
    local x      = 0.01 * (110 - types.NPC.stats.skills.enchant(self).modified)
    local charge = (x * ench)
    return charge
end

local function updateFacedObject()
    local time = core.getSimulationTime()

    if facedObjectUpdateTime < time then
        facedObjectUpdateTime = time

        local screenPos = util.vector2(0.5, 0.5)
        local worldVector = camera.viewportToWorldVector(screenPos)
        local origin = camera.getPosition()
        local activationDistance = 300
        local cameraDistance = camera.getThirdPersonDistance()
        activationDistance = activationDistance + cameraDistance
        local targetPoint = origin + worldVector * activationDistance
        facedObject = nearby.castRenderingRay(origin, targetPoint)

        if facedObject and facedObject.hit then
            local hitVector = facedObject.hitPos - origin
            distanceToFacedObject = hitVector:length() - cameraDistance
        else
            distanceToFacedObject = -1
        end
    end
end

local function getFacedObject()
    updateFacedObject()
    if (facedObject and facedObject.hit) then
        return facedObject
    else
        return nil
    end
end
local waitCount = 0
local function onUpdate()
    if (tempSpell ~= nil and types.Actor.getSelectedSpell(self) and types.Actor.getSelectedSpell(self).id ~= tempSpell.id) then
        if (waitCount > 10) then
            print(tempSpell.id)
            print(types.Actor.getSelectedSpell(self).id)
            tempSpell = nil
            prevSpell = nil
            print("removing saved spell")
            waitCount = 0
        end
        waitCount = waitCount + 1
    elseif (tempEnch ~= nil and types.Actor.getSelectedEnchantedItem(self) ~= tempEnch) then
        if (waitCount > 10) then
            tempEnch = nil
            prevEnch = nil
            print("removing saved ench")
            waitCount = 0
        end
        waitCount = waitCount + 1
    end
    lastStance = types.Actor.getStance(self)
end

local function calculateUnlockChance(player, pick, lockLevel)
    local fatigueBase = types.Actor.stats.dynamic.fatigue(player).base
    local fatigueCurrent = types.Actor.stats.dynamic.fatigue(player).current

    local normalisedFatigue = fatigueCurrent / fatigueBase
    local fFatigueBase = 1.25
    local fFatigueMult
    if fatigueBase == fatigueCurrent then
        fFatigueMult = 1.25
    elseif fatigueCurrent == 0 then
        fFatigueMult = 0
    else
        fFatigueMult = 1.25 - (1.25 / fatigueBase) * fatigueCurrent
    end
    local fatigueTerm = fFatigueBase - fFatigueMult * (normalisedFatigue)
    local x = 0.2 * types.Actor.stats.attributes.agility(player).modified +
        0.1 * types.Actor.stats.attributes.luck(player).modified + types.NPC.stats.skills.security(player).modified
    x = x * pick.type.record(pick).quality * fatigueTerm
    x = x + core.getGMST("fPickLockMult") * lockLevel

    local roll = math.random(100)

    return x, roll <= x
end


local function getSpellLockLevel(spell, effect)
    for index, eff in ipairs(spell.effects) do
        if (eff.effect.id == effect) then
            return eff.magnitudeMin
        end
    end
end
local function findSpellWithLowestLock(validEnchantments, effect)
    local lowestLockLevel = math.huge
    local enchantmentWithLowestLockLevel = nil
    local rindex = 0
    for index, enchantment in ipairs(validEnchantments) do
        local lockLevel = getSpellLockLevel(enchantment, effect)

        if lockLevel < lowestLockLevel then
            lowestLockLevel = lockLevel
            enchantmentWithLowestLockLevel = enchantment
            rindex = index
        end
    end

    return rindex, enchantmentWithLowestLockLevel
end
local function equipRequestedEffect(effect, magnitude, onSelf)
    print(effect, magnitude, onSelf)
    local validEnchantItems = {}
    local validEnchantments = {}
    local validEnchantItemsScroll = {}
    local validEnchantmentsScroll = {}
    local range1, range2

    if (onSelf) then
        range1 = 0
        range2 = 0
    else
        range1 = 1
        range2 = 2
    end
    local validSpells = {}
    if (magnitude == 0 or magnitude == nil) then
        magnitude = 1
    end
    for index, item in ipairs(types.Actor.inventory(self):getAll()) do
        if item.type.records[item.recordId].enchant ~= nil and item.type.records[item.recordId].enchant ~= "" then
            local enchant = item.type.records[item.recordId].enchant
            local enchSpell = core.magic.enchantments[item.type.records[item.recordId].enchant]
            for index, eff in ipairs(enchSpell.effects) do
                local enchCost = getObjectCastCost(item)
                if (eff.effect.id == effect and eff.magnitudeMin >= magnitude and (eff.range == range1 or eff.range == range2)) then
                    --   types.Actor.setSelectedEnchantedItem(self,item)
                    if (item.type == types.Book) then
                        table.insert(validEnchantItemsScroll, item)
                        table.insert(validEnchantmentsScroll, enchSpell)
                        print("found scroll")
                        break
                    elseif types.Item.getEnchantmentCharge(item) == -1 or types.Item.getEnchantmentCharge(item) >= enchCost then
                        table.insert(validEnchantItems, item)
                        table.insert(validEnchantments, enchSpell)
                        break
                    end
                end
            end
        end
    end
    for index, spell in ipairs(types.Actor.spells(self)) do
        for index, eff in ipairs(spell.effects) do
            if (eff.effect.id == effect and eff.magnitudeMin >= magnitude and spell.cost <= types.Actor.stats.dynamic.magicka(self).current) then
                table.insert(validSpells, spell)
                break
            end
        end
    end
    if #validEnchantItems > 0 then
        local index, ench = findSpellWithLowestLock(validEnchantments, effect)
        types.Actor.setSelectedEnchantedItem(self, validEnchantItems[index])
        tempEnch = validEnchantItems[index]
        return true
    end
    if #validEnchantItemsScroll > 0 then
        local index, spell = findSpellWithLowestLock(validEnchantmentsScroll, effect)
        types.Actor.setSelectedEnchantedItem(self, validEnchantItemsScroll[index])
        tempEnch = validEnchantItemsScroll[index]
        return true
    end
    if #validSpells > 0 then
        local index, spell = findSpellWithLowestLock(validSpells, effect)
        types.Actor.setSelectedSpell(self, spell)
        tempSpell = spell
        return true
    end
    return false --no spell found
end
local function getEffectApplied(effectId)
    local effect = types.Actor.activeEffects(self):getEffect(effectId)
    return effect and effect.magnitude > 0
end
local function ApplyEffectAndSave(effectId, level, onSelf)
    local prevSpellTemp = types.Actor.getSelectedSpell(self)
    local prevEnchTemp = types.Actor.getSelectedEnchantedItem(self)
    if equipRequestedEffect(effectId, level, onSelf) then
        prevSpell = prevSpellTemp
        prevEnch = prevEnchTemp
    end
end

local function goIntoSpell()
    updateFacedObject()
    local fob = nil
    if (getFacedObject() and getFacedObject().hitObject) then
        fob = getFacedObject().hitObject
        if (fob and fob.type.record(fob).id) then
            if (fob.type == types.Door or fob.type == types.Container and types.Lockable.getLockLevel(fob) > 0 and types.Lockable.isLocked(fob)) then
                local level = types.Lockable.getLockLevel(fob)
                ApplyEffectAndSave("open", level, false)
                return
            end
        end
    end
    if getEffectApplied("poison") then
        ApplyEffectAndSave("curepoison", 1, true)
        return
    end
    if (self.cell.hasWater) then
        if (self.position.z < -105 and not getEffectApplied("waterbreathing") and (fob == nil or fob.type ~= types.NPC or fob.type ~= types.Creature)) then
            ApplyEffectAndSave("waterbreathing", 1, true)
            return
        elseif self.position.z < 0 then
            ApplyEffectAndSave("waterwalking", 1, true)
            return
        end
    end
    local z, y, x = self.rotation:getAnglesZYX()
    if x < -1.34 then
        ApplyEffectAndSave("levitate", 1, true)
    end
end
local function goIntoWeapon()
    updateFacedObject()
    local fob = nil
    if (getFacedObject() and getFacedObject().hitObject) then
        fob = getFacedObject().hitObject
        if (fob and fob.type.record(fob).id) then
            if (fob.type == types.Door or fob.type == types.Container and types.Lockable.getLockLevel(fob) > 0 and types.Lockable.isLocked(fob)) then
                --switch to lockpick
                for index, value in ipairs(types.Actor.inventory(self):getAll(types.Lockpick)) do
                    if calculateUnlockChance(self,value,types.Lockable.getLockLevel(fob) == 100) then
                        
                    end
                end
            elseif fob.type == types.Door or fob.type == types.Container and types.Lockable.getTrapSpell(fob) then
                --switch to probe
                for index, value in ipairs(types.Actor.inventory(self):getAll(types.Probe)) do
                    
                end
            end
        end
    end
end
local function returnToNormal()
    if prevSpell ~= nil then
        types.Actor.setSelectedSpell(self, prevSpell)
        prevSpell = nil
    elseif prevEnch ~= nil then
        types.Actor.setSelectedEnchantedItem(self, prevEnch)
        prevEnch = nil
    end
    tempSpell = nil
    tempEnch = nil
end

local function onInputAction(id)
    if (self.controls.use == 1) then
        return
    end
    if id == input.ACTION.ToggleSpell then
        local stance = types.Actor.getStance(self)
        if stance == lastStance then
            return
        end
        if stance == types.Actor.STANCE.Spell then
            goIntoSpell()
            ui.showMessage("Swiching To Spell")
        elseif stance == types.Actor.STANCE.Nothing or stance == types.Actor.STANCE.Weapon then
            ui.showMessage("Swiching From Spell")
            returnToNormal()
        end
    elseif id == input.ACTION.ToggleWeapon then
        local stance = types.Actor.getStance(self)
        if stance == types.Actor.STANCE.Weapon then
            goIntoWeapon()
            ui.showMessage("Swiching To Spell")
        elseif stance == types.Actor.STANCE.Nothing or stance == types.Actor.STANCE.Spell then
            ui.showMessage("Swiching From Spell")
            returnToNormal()
        end
    end

end

return {
    interfaceName = "zhac_TMaster_ccast",
    interface = {
        version = 1,
        onInit = onInit,
        getObjectCastCost = getObjectCastCost,
        getEffectApplied = getEffectApplied,
        calculateUnlockChance = calculateUnlockChance,
    },
    engineHandlers = {
        onInit = onInit,
        onFrame = onFrame,
        onInputAction = onInputAction,
        onUpdate = onUpdate,
    },
    eventHandlers = {
        TweakMasterUpdateSetting = TweakMasterUpdateSetting,
        sendPlayer = sendPlayer,
    }
}
