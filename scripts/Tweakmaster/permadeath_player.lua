local ui = require("openmw.ui")
local storage = require("openmw.storage")
local core = require("openmw.core")
local types = require("openmw.types")
local self = require("openmw.self")
local I = require("openmw.interfaces")

local permadeathData = storage.globalSection('permadeathData')
local permadeathState = { nonPermaDeath = 1, permaDeath = 2, dead = 3 }

local function PDeath_ShowMessage(message)
    ui.showMessage(message)
end
local wasKilled = false
local function onUpdate(dt)
    if wasKilled then
        return
    end
    local dead = types.Actor.stats.dynamic.health(self).current == 0
    if dead then
        local gameId = I.zhac_TMaster.getGameId()
        if permadeathData:get(gameId) == permadeathState.permaDeath then
            
            core.sendGlobalEvent("onPlayerDeath")
            wasKilled = true
        end
    end
end
local function onSave()
    local gameId = I.zhac_TMaster.getGameId()
    if gameId then
        if permadeathData:get(gameId) == permadeathState.nonPermaDeath then
            print("Death is certain")
            core.sendGlobalEvent("makePlayerPermaDeath")
        end
    end
end
local function onLoad()
 
end

return {
    eventHandlers = { PDeath_ShowMessage = PDeath_ShowMessage },
    engineHandlers = { onSave = onSave, onUpdate = onUpdate }
}
