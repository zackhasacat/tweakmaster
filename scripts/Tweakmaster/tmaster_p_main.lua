local core = require("openmw.core")
local I = require("openmw.interfaces")
local input = require("openmw.input")
local types = require("openmw.types")
local async = require("openmw.async")
local storage = require("openmw.storage")
local ui = require("openmw.ui")
local self = require("openmw.self")
local permadeathData = storage.globalSection('permadeathData')
local permadeathSettings = storage.playerSection('permadeathData')
local permadeathState = { nonPermaDeath = 1, permaDeath = 2, dead = 3 }
local gameId = nil
local function TMShowMessage(m)
    ui.showMessage(m)
end
local settingsRegistered = false
local function registerPDSettings()
    if settingsRegistered == true then return end
    if gameId ~= nil then
        if permadeathData:get(I.zhac_TMaster.getGameId()) == permadeathState.dead then
            types.Actor.stats.dynamic.health(self).current = 0
            ui.showMessage(
                "This character's adventure has ended. Start a new game to begin anew.")
            core.sendGlobalEvent("freezeGame")
        elseif permadeathData:get(I.zhac_TMaster.getGameId()) == permadeathState.permaDeath then
            I.Settings.registerGroup {
                key = "SettingsTweakMasterPD",
                page = "TweakMaster",
                l10n = "TweakMaster",
                name = 'Permadeath Settings',
                permanentStorage = false,
                description =
                "This character already has permadeath enabled. You can't disable it.",

            }
            settingsRegistered = true
        else
            I.Settings.registerGroup {
                key = "SettingsTweakMasterPD",
                page = "TweakMaster",
                l10n = "TweakMaster",
                name = 'Permadeath Settings',
                permanentStorage = false,
                settings = {
                    {
                        key = 'EnablePD',
                        renderer = 'checkbox',
                        name = 'Enable Permadeath for this Character',
                        description =
                        'When this is turned on, your character\'s saves will only be accessible before they die. This setting may be turned on at any time, and after it is turned on, it may be turned off again before the next time you save. However, after you save, the setting will be permenetly enabled, and if your character dies, loading any previous saves from that character will be prevented. ',
                        default = false,
                    }

                }
            }
            settingsRegistered = true
            permadeathSettings:set("EnablePD", false)
        end
    else
        I.Settings.registerGroup {
            key = "SettingsTweakMasterPD",
            page = "TweakMaster",
            l10n = "TweakMaster",
            name = 'Permadeath Settings',
            permanentStorage = false,
            description =
            "You can't turn on permadeath for this character, you must create a new one. (You may only use characters created after this mod was installed)",
            settings = {
                {
                }

            }
        }
        settingsRegistered = true
    end
end
local function onLoad(data)
    if data then
        gameId = data.gameId
    end
    registerPDSettings()
end
local function onInit()

end

local function onSave()
    if gameId then
        return { gameId = gameId }
    end
end
local function setGameID(id)
    gameId = id
    registerPDSettings()
end
local function getGameId()
    return gameId
end
core.sendGlobalEvent("sendPlayer", self)

return {
    interfaceName = "zhac_TMaster",
    interface = {
        version = 1,
        onInit = onInit,
        getGameId = getGameId,
    },
    engineHandlers = {
        onSave = onSave,
        onLoad = onLoad
    },
    eventHandlers = {
        TMShowMessage = TMShowMessage,
        setGameID = setGameID,
    }
}
