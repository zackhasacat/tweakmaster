local gameId = nil
local storage = require("openmw.storage")
local types = require("openmw.types")
local world = require("openmw.world")
local core = require("openmw.core")
local permadeathData = storage.globalSection('permadeathData')
local permadeathState = { nonPermaDeath = 1, permaDeath = 2, dead = 3 }
local function generateRandomString(length)
    local chars = {}
    for i = 1, length do
        local charType = math.random(3)
        local charCode
        if charType == 1 then
            charCode = math.random(48, 57)  -- Random number
        elseif charType == 2 then
            charCode = math.random(65, 90)  -- Uppercase letter
        else
            charCode = math.random(97, 122) -- Lowercase letter
        end
        table.insert(chars, string.char(charCode))
    end
    return table.concat(chars)
end

math.randomseed(os.time())
local function onNewGame()
    gameId = generateRandomString(25)
    print("New game started!", gameId)
    permadeathData:set(gameId, permadeathState.nonPermaDeath)
end
local function onPlayerAdded(player)
    if gameId then
        player:sendEvent("setGameID", gameId)
        if permadeathData:get(gameId) == permadeathState.dead then
        end
    end
end
local function onLoad(data)
    world.setSimulationTimeScale(1)
    if data then
        gameId = data.gameId
    end
    if gameId then
        print("Game loaded.", gameId)
 
    end
end
local function freezeGame()
world.setSimulationTimeScale(0)
end
local function makePlayerPermaDeath()
    if not gameId then return end
    permadeathData:set(gameId, permadeathState.permaDeath)
end
local function onPlayerDeath()
    permadeathData:set(gameId, permadeathState.dead)
end
local function onSave()
    return { gameId = gameId }
end
local function getID()
    return gameId
end

return {
    interfaceName = "Permadeath",
    interface = { getID = getID },
    engineHandlers = { onPlayerAdded = onPlayerAdded, onSave = onSave, onLoad = onLoad, onNewGame = onNewGame },
    eventHandlers = { freezeGame = freezeGame,makePlayerPermaDeath = makePlayerPermaDeath, onPlayerDeath = onPlayerDeath }
}
