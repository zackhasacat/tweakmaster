local types = require("openmw.types")
local world = require("openmw.world")
local acti = require("openmw.interfaces").Activation
local core = require("openmw.core")
local util = require("openmw.util")
local storage = require("openmw.storage")

local function onLoad(data)
    if not data then return end
end

local function removeOwnership(cell)
    if cell.name == nil then
        cell = world.getCellByName(cell)
    end
    for index, obj in ipairs(cell:getAll()) do
        obj.owner.recordId = nil
        obj.owner.factionId = nil
    end
end
local blackListTypes = { [types.Door] = true }
local blackListPrefix = {"in_","t_imp_set","ab_in","^_"}
local function startsWith(str, prefix) --Checks if a string starts with another string
    return string.sub(str, 1, string.len(prefix)) == prefix
end
local function deFurnishCell(cell)
    if cell.name == nil then
        cell = world.getCellByName(cell)
    end
    for index, obj in ipairs(cell:getAll()) do
        local valid = true
        if obj.type == types.Door then 
            valid = false
        else
            for index, value in ipairs(blackListPrefix) do
                if startsWith(obj.recordId,value) then
                    valid = false
                end
            end
        end
        if valid then
            local bbox = obj:getBoundingBox().halfSize
            if bbox.x > 120 or bbox.y > 120 or bbox.z > 120 then
                valid = false
            end
            
        end
        if obj.baseType == types.Item then
           valid = true
        end
        if valid then
            if obj.baseType ~= types.Item then
                print(obj.recordId)
            end
            obj:remove()
        end
    end
end
local function onSave()
    return {}
end
return {
    interfaceName  = "HouseBuy",
    interface      = {
        version = 1,
        deFurnishCell = deFurnishCell,
        removeOwnership = removeOwnership,

    },
    engineHandlers = {
        onSave = onSave,
        onLoad = onLoad,
        onActorActive = onActorActive
    },
    eventHandlers  = {
    }
}
