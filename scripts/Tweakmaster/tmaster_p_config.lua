local core = require("openmw.core")
local I = require("openmw.interfaces")
local input = require("openmw.input")
local async = require("openmw.async")
local storage = require("openmw.storage")
I.Settings.registerPage {
    key = "TweakMaster",
    l10n = "TweakMaster",
    name = "Tweak Master - Main",
}

local key = nil
local function boolSetting(key, name, default, desc)
    print(key)
    if not default then
        default = false
    end
    return {
        key = key,
        renderer = 'checkbox',
        name = name,
        default = default,
        description = desc
    }
end
local function boolSettingEnable(key, name, default, desc)
    print(key)
    if not default then
        default = false
    end
    return {
        key = "Enable" .. key,
        renderer = 'checkbox',
        name = "Enable " .. name,
        default = default,
        description = desc
    }
end
--Tweaks:
--Quicktoss
--Autoloot
--Auto Skooma Hide
--Guard Stop
--AutoRA
--Context Spell
--Disable Distant Objects

--Disable Cliff Racers
key = "SettingsTweakMaster"
I.Settings.registerGroup {
    key = "SettingsTweakMaster",
    page = "TweakMaster",
    l10n = "TweakMaster",
    name = 'Main Settings',
    permanentStorage = true,
    settings = {
        boolSettingEnable('Quicktoss', 'Quicktoss', false),
        boolSettingEnable('AutoSkoomaHide', 'Auto Skooma Hide', false),
        boolSettingEnable('GuardStop', 'Guard Stop', false),
        boolSettingEnable('AutoRA', 'Auto Actor Reset', false),
        boolSettingEnable('ContextSpell', 'Spell Selection Context', false),
        boolSettingEnable('PreventExtinguish', 'Prevent Light Extinguishing', false),
        boolSetting ('DisableCliff', 'Disable Cliff Racers', false, "Will block all and any cliff racers from existing."),
        boolSettingEnable('AutoLoot', 'Autoloot', false)
    }
}

local SettingsTweakMaster = storage.playerSection("SettingsTweakMaster")
local SettingsPermaDeath = storage.playerSection("SettingsTweakMasterPD")
local QuicktossLoaded, AutoSkoomaHideLoaded, GuardStopLoaded, AutoRALoaded, ContextSpellLoaded, PreventExtinguishLoaded, AutoLootLoaded, disableCliffLoaded =
    false, false, false, false, false, false, false, false
I.Settings.registerPage {
    key = 'SettingsTweakMasterModules',
    l10n = 'AutoLootl10n',
    name = 'Tweak Master - Modules',
    description = 'Settings for enabled Modules are held here. If no enabled module has settings, this will be blank.',
}
local function updateGroups()
    if not AutoLootLoaded and SettingsTweakMaster:get("EnableAutoLoot") then
        AutoLootLoaded = true

        I.Settings.registerGroup {
            key = 'AutoLoot',
            page = 'SettingsTweakMasterModules',
            l10n = 'AutoLootl10n',
            name = 'Autoloot',
            permanentStorage = false,
            settings = {
                {
                    key = 'DistanceLoot',
                    renderer = 'number',
                    name = 'Loot Distance',
                    description = 'Distance to loot from',
                    default = 200,
                },
                {
                    key = 'ValtoWeightRatio',
                    renderer = 'number',
                    name = 'Minimum Value to Weight Ratio',
                    description =
                    "How much an object must be worth proportiate to it's weight in points. If the item weighs 1lb, and is worth 100 gold, it has a ratio of 100.",
                    default = 100,
                },
                {
                    key = 'InventoryReserve',
                    renderer = 'number',
                    name = 'Percent of Inventory to Reserve',
                    description =
                    'Will stop collecting items with weight once you fill your inventory to this point. If you set it to 90, you will collect items until your inventory is at 90% capacity.',
                    default = 100,
                },
                {
                    key = 'preventOverEncumber',
                    renderer = 'checkbox',
                    name = 'Prevent Over-Encumbering',
                    description =
                    "If enabled, you will never loot an item if that item's weight would cause you to become over-encumbered",
                    default = true,
                },
                {
                    key = 'InventoryReserveMax',
                    renderer = 'number',
                    name = 'Maximum Weight of Items to collect with Inventory Reserve active',
                    description =
                    'After you reach the above threshold, you will be only collect items with this weight value or less. Take the above setting in mind as well.',
                    default = 0.1,
                },
                {
                    key = 'ShowLootMessage',
                    renderer = 'checkbox',
                    name = 'Show Loot Message',
                    description =
                    'If enabled, you will see a messagebox come up whenever an item is looted automatically.',
                    default = true,
                },
                {
                    key = 'AlwaysLootIngredCreat',
                    renderer = 'checkbox',
                    name = 'Always Loot Ingredients from Creatures',
                    description =
                    'Should Ingredients be picked up from slain creatures, regardless of weight? Will only work if Loot Creatures is enabled.',
                    default = true,
                },
                {
                    key = 'AlwaysTakeWeightless',
                    renderer = 'checkbox',
                    name = 'Always Loot Weightless Items',
                    description = 'Should items be picked up if they have no weight, regardless of value?',
                    default = true,
                },
                {
                    key = 'LootEquip',
                    renderer = 'checkbox',
                    name = 'Loot Equipment',
                    description = 'Should items equipped by your target be looted?',
                    default = 'false',
                },
                {
                    key = 'LootCreatures',
                    renderer = 'checkbox',
                    name = 'Loot Creatures',
                    description = 'Should Creatures be looted?',
                    default = true,
                },
                {
                    key = 'LootNPCs',
                    renderer = 'checkbox',
                    name = 'Loot NPCs',
                    description = 'Should NPCs be looted?',
                    default = true,
                },
                {
                    key = 'lootLine',
                    renderer = 'textLine',
                    name = 'Loot Message Prefix',
                    description =
                    'This is the start of the line of the message you will get when messages are enabled. After this line will be a space, the count(if it is above 1), and then the item name',
                    default = "You collected",
                },
            },
        }
    end
    if not disableCliffLoaded and SettingsTweakMaster:get("DisableCliff") then
        disableCliffLoaded = true
        I.Settings.registerGroup {
            key = 'SettingsTweakMasterCliff',
            page = 'SettingsTweakMasterModules',
            l10n = 'AutoLootl10n',
            name = 'Disable Cliff Racers',
            permanentStorage = false,
            settings = {
                {
                    key = 'CLDespawnChance',
                    renderer = 'number',
                    name = 'Cliff Racer Despawn Chance',
                    description =
                    'The chance for Cliff Racers to be despawned when they load. If 100, they will always despawn while the module is enabled.',
                    default = 100,
                }, }
        }
    end
end
updateGroups()
local function updateKeyset(section, key)
    if key then
        updateGroups()
        core.sendGlobalEvent("TweakMasterUpdateSetting",
            { key = key, value = storage.playerSection(section):get(key), section = section })
    end
end
SettingsPermaDeath:subscribe(async:callback(updateKeyset))
SettingsTweakMaster:subscribe(async:callback(updateKeyset))
storage.playerSection("SettingsTweakMasterCliff"):subscribe(async:callback(updateKeyset))
