local core = require('openmw.core')
local util = require('openmw.util')
local self = require('openmw.self')
local nearby = require('openmw.nearby')
local async = require('openmw.async')
local types = require('openmw.types')

local Actor = types.Actor
local isKilled = false
local player = nil
local function objectBlacklisted(obj)
  if obj.recordId:find("bound") or obj.recordId:find("summon") then
    return true
  else
    return false
  end
end
local function getPlayer()
  for _, actor in ipairs(nearby.actors) do
    if actor.type == types.Player then
      player = actor
      return
    end
  end
end
local lastHealth = 1
local function onUpdate(dt)
  local health = types.Actor.stats.dynamic.health(self).current

  if health == 0 and not isKilled and (lastHealth > 0) then
    local eventData = {
      player = nearby.players[1],
      self = self,
    }
    if (not objectBlacklisted(self)) then
      nearby.players[1]:sendEvent("SendNotification", eventData)
    end
    isKilled = true
  elseif health > 0 then
    -- print("Living")
  end

  lastHealth = health
end
return {
  engineHandlers = {
    onUpdate = onUpdate,
    onLoad = onLoad
  }
}
