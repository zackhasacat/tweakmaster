local camera = require('openmw.camera')
local core = require('openmw.core')
local input = require('openmw.input')
local util = require('openmw.util')
local self = require('openmw.self')
local types = require('openmw.types')
local ui = require('openmw.ui')
local async = require('openmw.async')
local storage = require('openmw.storage')
local I = require('openmw.interfaces')

--TODO:
--Block looting bound items
--Block looting summoned creatures
local playerSettings = storage.playerSection('SettingsPlayerMyMod')
local function FindWeight(item)
    return item.type.records[item.recordId].weight
end
local function FindName(item)
    return item.type.records[item.recordId].name
end
local function FindValue(item)
    return item.type.records[item.recordId].value
end
local SettingsTweakMaster = storage.playerSection("SettingsTweakMaster")
if SettingsTweakMaster:get("EnableAutoLoot") then
   
end
local function objectBlacklisted(obj)
    if obj.recordId:find("bound") or obj.recordId:find("summon") then
        return true
    else
        return false
    end
end

local function getMagicWeight()
    local effect = types.Actor.activeEffects(self):getEffect("feather")
    local weightOffset = 0
    if (effect) then
        weightOffset = weightOffset - effect.magnitude
    end
    local effect = types.Actor.activeEffects(self):getEffect("burden")
    if (effect) then
        weightOffset = weightOffset + effect.magnitude
    end
    return weightOffset
end
local function SendNotification(eventData)
    local item = eventData.object
    local player = eventData.player
    local object = eventData.self
    local slots = {
        types.Actor.EQUIPMENT_SLOT.Ammunition,
        types.Actor.EQUIPMENT_SLOT.Amulet,
        types.Actor.EQUIPMENT_SLOT.Belt,
        types.Actor.EQUIPMENT_SLOT.Boots,
        types.Actor.EQUIPMENT_SLOT.CarriedLeft,
        types.Actor.EQUIPMENT_SLOT.CarriedRight,
        types.Actor.EQUIPMENT_SLOT.Cuirass,
        types.Actor.EQUIPMENT_SLOT.Greaves,
        types.Actor.EQUIPMENT_SLOT.Helmet,
        types.Actor.EQUIPMENT_SLOT.LeftGauntlet,
        types.Actor.EQUIPMENT_SLOT.LeftPauldron,
        types.Actor.EQUIPMENT_SLOT.LeftRing,
        types.Actor.EQUIPMENT_SLOT.Pants,
        types.Actor.EQUIPMENT_SLOT.RightGauntlet,
        types.Actor.EQUIPMENT_SLOT.RightPauldron,
        types.Actor.EQUIPMENT_SLOT.RightRing,
        types.Actor.EQUIPMENT_SLOT.Robe,
        types.Actor.EQUIPMENT_SLOT.Shirt,
        types.Actor.EQUIPMENT_SLOT.Skirt
    }
    local playerSettings = storage.playerSection('AutoLoot')
    local inventory = types.Actor.inventory(object)
    local dist = (object.position - player.position):length()
    local lootEquip = playerSettings:get('LootEquip')
    local valueRatio = playerSettings:get('ValtoWeightRatio')

    local LootCreatures = playerSettings:get('LootCreatures')
    local LootNPCs = playerSettings:get('LootNPCs')

    if (object.type == types.NPC and LootNPCs == false) then
        return
    elseif (object.type == types.Creature and LootCreatures == false) then
        return
    end
    local equip = types.Actor.equipment(object)
    local strength = types.Actor.stats.attributes.strength(self).modified
    local maxCarryWeight = strength * 5

    local plrinventory = types.Actor.inventory(self)
    local totalWeight = 0
    for _, item in ipairs(plrinventory:getAll()) do
        local itemWeight = FindWeight(item)
        totalWeight = totalWeight + (itemWeight * item.count)
    end
    totalWeight = totalWeight + getMagicWeight()
    local weightPercentage = (totalWeight / maxCarryWeight) * 100
    print(
        "player weight is " .. math.ceil(totalWeight) .. " out of " .. maxCarryWeight .. " " .. weightPercentage)
    local skipWeightedItems = false
    if (weightPercentage > tonumber(playerSettings:get('InventoryReserve'))) then
        skipWeightedItems = true
    end
    if dist < playerSettings:get('DistanceLoot') then
        for _, item in ipairs(inventory:getAll()) do
            if (not objectBlacklisted(item)) then
                -- Check if lootEquip is false and equip table does not contain item
                local skipLoot = false
                if not lootEquip then
                    for _, equipItem in ipairs(slots) do
                        if item == types.Actor.equipment(object, equipItem) then
                            skipLoot = true
                            break
                        end
                    end
                end
                local function shouldUseAn(itemName)
                    -- Check if the first letter of the item name is a vowel sound
                    local firstLetter = itemName:sub(1, 1):lower()
                    return firstLetter == "a" or firstLetter == "e" or firstLetter == "i" or firstLetter == "o" or
                        firstLetter == "u"
                end

                local function getArticle(itemName)
                    -- Determine the appropriate article based on the sound of the item name
                    if shouldUseAn(itemName) then
                        return "an"
                    else
                        return "a"
                    end
                end

                local itemValue = FindValue(item)
                local itemWeight = FindWeight(item)

                if (itemWeight > tonumber(playerSettings:get('InventoryReserveMax'))) then
                    if (skipWeightedItems) then
                        skipLoot = true
                    end
                end
                if (itemWeight  + totalWeight >= maxCarryWeight and (playerSettings:get('preventOverEncumber'))) then
                  --If item weight would bring us over the limit, and the setting to prevent this is on, we skip it
                        skipLoot = true
                end
                if (itemWeight > 0) then
                    local valueToWeightRatio = itemValue / itemWeight
                    if (valueToWeightRatio < tonumber(valueRatio)) then
                        skipLoot = true
                    end
                end
                if (playerSettings:get('AlwaysLootIngredCreat') == true and object.type == types.Creature and item.type == types.Ingredient) then
                    skipLoot = false
                end
                if not skipLoot then
                    if (playerSettings:get('ShowLootMessage')) then
                        if item.count == 1 then
                            local itemName = FindName(item)
                            local article = getArticle(itemName)
                            ui.showMessage(playerSettings:get('lootLine').. " " .. itemName)
                        elseif item.count > 1 then
                            ui.showMessage(playerSettings:get('lootLine').. " " .. item.count .. " " .. FindName(item))
                        end
                    end
                    local eventData = {
                        item = item,
                        player = player,
                        self = self,
                    }
                    core.sendGlobalEvent("TransferObject2", eventData)
                end
            end
        end
    end
end
return {
    eventHandlers = {
        SendNotification = SendNotification
    }
}
