local types = require("openmw.types")
local acti = require("openmw.interfaces").Activation
local goldTypes = { "gold_001", "gold_005", "gold_010", "gold_025", "gold_100" }
local function isOwned(object)
    if object.ownerRecordId ~= nil and object.ownerFactionId ~= nil then return true end
    return false
end
local function isGold(object)
    for index, value in ipairs(goldTypes) do
        if object.recordId == value then
            return true
        end
    end
    return false
end
local function activateGold(object, actor)
    if isGold(object) and not isOwned(object) then
        for index, obj in ipairs(object.cell:getAll(types.Miscellaneous)) do
            if isGold(obj) and not isOwned(obj) and obj ~= object then
                obj:moveInto(types.Actor.inventory(actor))
            end
        end
    end
end


acti.addHandlerForType(types.Miscellaneous, activateGold)
return {
    interfaceName = "zhac_TMaster",
    interface = {
        version = 1,
    },
    engineHandlers = {
    },
    eventHandlers = {
    }
}
